<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use  illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\user;

class AuthController extends Controller
{
    //creating user
    public function signup(Request $request){

        $request->validate([
            'name'=>'required|string',
            'email'=>'required|string|email|unique:users',
            'password'=>'required|string|confirmed'
        ]);

        $user=new User([
            'name'=>$request->name,
            'email'=>$request->email,
            'password'=>bcrypt($request->password)
        ]);

        $user->save();

        return response()->json([
            'message'=>'User Created Successfully!'
        ], 201);
    }
    
    
    //login user and create token
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);


        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();
        return response()->json([
            'access_token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }

    //Logout user, revoking the token
    public function logout(Request $request)
    {

        if(\Auth::check()){
            $request->user()->token()->revoke();
            return response()->json([
                'message' => 'Successfully logged out'
            ]);
        }else{
            return response()->json([
                'message' => 'not logged in'
            ]);
        }
    }

    

    //get the authenticated user
    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
